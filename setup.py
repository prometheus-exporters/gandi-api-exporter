import sys
from setuptools import setup

if sys.version_info.major < 3:
  raise RuntimeError('Installing requires Python 3 or newer')
setup(
  name='prometheus-gandi-api-exporter',
  version='0.1.0',
  description='Prometheus Exporter collecting metrics from Gandi API v5',
  packages=[
    'src',
  ],
  install_requires=[
    'prometheus-client',
    'requests',
    'flask',
    'python-dateutil',
    'uwsgi'
  ],
)
