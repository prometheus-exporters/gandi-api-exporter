import json
import sys
import requests
import dateutil.parser

from prometheus_client import CollectorRegistry, Gauge, make_wsgi_app
from flask import Flask
from werkzeug.middleware.dispatcher import DispatcherMiddleware

registry = CollectorRegistry()

created_at = Gauge('gandi_domain_created_at', 'description soon', ['instance'], registry=registry)
registry_created_at = Gauge('gandi_domain_registry_created_at', 'description soon', ['instance'], registry=registry)
registry_ends_at = Gauge('gandi_domain_registry_ends_at', 'description soon', ['instance'], registry=registry)
updated_at = Gauge('gandi_domain_updated_at', 'description soon', ['instance'], registry=registry)

apikey = sys.argv[1]
headers = {'Authorization': 'Apikey %s' % (apikey,)}


def get_domains():
    response = requests.get('https://api.gandi.net/v5/domain/domains?per_page=30000', headers=headers)
    return response.json()


def get_domains_to_ignore():
    try:
        with open('./domains_to_ignore.json') as f:
            return json.load(f)
    except FileNotFoundError:
        return []


domains_to_ignore = get_domains_to_ignore()


def register_domains_metric(domains):
    for domain in domains:
        if domain['fqdn'] in domains_to_ignore:
            continue

        for key,value in domain['dates'].items():
            timestamp = dateutil.parser.parse(value).timestamp()
            globals()[key].labels(instance=domain['fqdn']).set(int(timestamp))


app = Flask(__name__)

@app.route('/probe')
def probe():
    register_domains_metric(get_domains())
    return make_wsgi_app(registry)


app.wsgi_app = DispatcherMiddleware(app.wsgi_app, {
    '/metrics': make_wsgi_app()
})
